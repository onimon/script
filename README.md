# OniMon - A simple Tor Relay Admin Dashboard.

## Overview

OniMon is a [web interface](https://gitlab.com/onimon/dashboard)
to display status information about a running Tor Relay.
It is inspired from the [Tor Arm](https://www.torproject.org/projects/arm.html.en)
project and based on the [Pi-Hole Admin Dashboard](https://github.com/pi-hole/AdminLTE).

The Dashboard executes the `onimon` script on the server to gather information from Tor.
The script opens a control port connection for each request and is used to fetch Tor
status, traffic data and network details. It is written in Python and requires the
[Stem](https://stem.torproject.org/) module.

Note that this script can also be used independently of the Dashboard to provide an
easily scriptable method to retrieve Tor information.

### Example output:

```
onimon version
> 0.2.9.9 (git-1d8323c042800718)

onimon address
> 1.2.3.4

onimon config ORPort
> 443
```


## Installation

### Requirements

* Tor instance: tor
* Python with STEM: python, python-stem

### Installation procedure

1. Copy [the script](https://gitlab.com/onimon/script/raw/master/onimon)
   and [the python module](https://gitlab.com/onimon/script/raw/master/_onimon.py)
   into `/usr/local/bin/` and set their permissions to `0755` and `0644`,
   respectively, so that the script can be executed by all users:

        wget -O /usr/local/bin/onimon https://gitlab.com/onimon/script/raw/master/onimon
        chmod 0755 /usr/local/bin/onimon
        wget -O /usr/local/bin/_onimon.py https://gitlab.com/onimon/script/raw/master/_onimon.py
        chmod 0644 /usr/local/bin/_onimon.py
        
2. You can speed up the script's execution delay by running the `onimon` script
   once in a writable directory (or with admin rights). This will create a
   precompiled file `_onimon.pyc` file that you can place into `/usr/local/bin`.
   Note that you must repeat this process after updating the python module.

3. Create a [configuration file](https://gitlab.com/onimon/script/raw/master/onimon.conf)
   named `/etc/onimon.conf` and set its permissions to `0640`. Change its group
   to make it accessible by the user running the webserver. On most systems,
   the correct username is `www-data`:

        touch /etc/onimon.conf
        chmod 0640 /etc/onimon.conf
        chown root:www-data /etc/onimon.conf

4. The config file allows you to specify options to reach to Tor's ControlPort,
   e.g. the port number and the authentification password. The script uses Tor's
   password authentication, which means you must define a `HashedControlPassword`
   in your `torrc` file (run `tor --hash-password <text>` to create a password
   hash, see the [Tor manual](https://www.torproject.org/docs/tor-manual.html.en)
   for details). Note that in the `onimon.conf` file you need to enter the plain
   (un-hashed) password.

5. Test if the webserver user can run the script and that it can reach Tor's
   control port:

        sudo -u www-data onimon version
        > 0.2.9.9 (git-1d8323c042800718)
