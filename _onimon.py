#!/usr/bin/env python
#
# OniMon - A simple Tor Relay Admin Dashboard.
# Copyright (c) 2017 Jan D. Behrens <zykure42@gmail.com>
#
# Python script to interface with the web dashboard.
#
# The OniMon script is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 2 of the
# License, or (at your option) any later version.

from stem.control import Controller
from stem.util import conf

COMMANDS = {
#    name              args | required
    'config':           ( 1,  0 ),
    'version':          ( 0,  0 ),
    'process':          ( 0,  0 ),
    'status':           ( 0,  0 ),
    'fingerprint':      ( 0,  0 ),
    'policy':           ( 1,  0 ),
    'listeners':        ( 1,  0 ),
    'address':          ( 0,  0 ),
    'traffic':          ( 1,  0 ),
    'nodes':            ( 0,  0 ),
    'lookup':           ( 1,  1 ),
    'describe':         ( 1,  1 ),
}

CONFIG_ITEMS = {
  'password':   "",
  'port':       9051,
}

def _config_validator(key, value):
  if key == 'password':
    return str(value)
  elif key == 'port':
    if not connection.is_valid_port(value):
      raise ValueError("'%s' isn't a valid port" % value)
    return int(value)

#######################################################################
## Tor control functions
class OniMonControl:

    def __init__(self, config):
        #try:
        if True:
            self.control = Controller.from_port(port=int(config.get('port')))
            if 'password' in config.keys():  # only if password defined
                self.control.authenticate(config.get('password'))
        #except:
        #    self.control = None

    def config(self, name=None):
        """Config value from torrc.

        GETINFO config-text
        GETCONF <name>

        Returns current value of the given item.
        Returns a complete list of values if no item is defined.
        """
        if not self.control:
            return
        if not name:
            try:
                values = self.control.get_info("config-text")
                print(values)
            except:
                return
        else:
            try:
                value = self.control.get_conf(name)
                print(value)
            except:
                return

    def version(self):
        """Tor version.

        GETINFO version
        """
        if not self.control:
            return
        try:
            version = self.control.get_info("version")
            print(version)
        except:
            print("unknown")

    def process(self):
        """Tor process ID.

        GETINFO process/pid
        """
        if not self.control:
            return
        try:
            pid = self.control.get_info("process/pid")
            print(pid)
        except:
            return

    def status(self):
        """Tor status.

        GETINFO status/reachability-succeeded/or

        Returns 1 if Tor's ORPort is reachable (reachability succeeded).
        Returns 0 otherwise.
        Returns negative value in case of errors.
        """
        if not self.control:
            return
        try:
            status = self.control.get_info("status/reachability-succeeded/or")
            print(status)
        except:
            print(-1)

    def fingerprint(self):
        """Tor fingerprint.

        GETINFO fingerprint
        """
        if not self.control:
            return
        try:
            fingerprint = self.control.get_info("fingerprint")
            print(fingerprint)
        except:
            return

    def policy(self, protocol=None):
        """Exit policy for given protocol.

        GETINFO exit-policy/{ipv4|ipv6|full}

        Returns full exit policy if no protocol is defined.
        """
        if not self.control:
            return
        try:
            if protocol == 'ipv4':
                policy = self.control.get_info("exit-policy/ipv4")
            elif protocol == 'ipv6':
                policy = self.control.get_info("exit-policy/ipv6")
            else:
                policy = self.control.get_info("exit-policy/full")
            print(policy)
        except:
            return

    def listeners(self, connection=None):
        """Listeners for given connection type.

        GETINFO net/listeners/{or|dir|socks|dns|control}

        Returns all listeners if no connection type is defined.
        """
        if not self.control:
            return
        try:
            if protocol == 'or':
                listeners = self.control.get_info("net/listeners/or")
            elif protocol == 'dir':
                listeners = self.control.get_info("net/listeners/dir")
            elif protocol == 'socks':
                listeners = self.control.get_info("net/listeners/socks")
            elif protocol == 'dns':
                listeners = self.control.get_info("net/listeners/dns")
            elif protocol == 'control':
                listeners = self.self.control.get_info("net/listeners/control")
            else:
                listeners = []
                listeners.append(self.control.get_info("net/listeners/or"))
                listeners.append(self.control.get_info("net/listeners/dir"))
                listeners.append(self.control.get_info("net/listeners/socks"))
                listeners.append(self.control.get_info("net/listeners/dns"))
                listeners.append(self.control.get_info("net/listeners/control"))
                listeners = '\n'.join(listeners)
            print(listeners)
        except:
            return

    def address(self):
        """Outbound IP address.

        GETINFO address
        """
        if not self.control:
            return
        try:
            address = self.control.get_info("address")
            print(address)
        except:
            return

    def traffic(self, mode=None):
        """Total traffic in bytes.

        GETINFO traffic/{read|written}

        Returns traffic count for the given direction (down/up).
        Returns both values if no direction is given.
        """
        if not self.control:
            return
        try:
            if mode == 'down':
                traffic = int(self.control.get_info("traffic/read"))
                print(traffic)
            elif mode == 'up':
                traffic = int(self.control.get_info("traffic/written"))
                print(traffic)
            else:
                traffic_r = int(self.control.get_info("traffic/read"))
                traffic_w = int(self.control.get_info("traffic/written"))
                print("%d %d" % (traffic_r, traffic_w))
        except:
            return

    def nodes(self):
        """ Tor node info.

        GETINFO ns/all

        Gathers information about all known OR identities.
        Returns information in Tor's dir-spec format.
        """
        try:
            info = self.control.get_info("ns/all")
            print(info)
        except:
            return

    def lookup(self, name):
        """Tor node info.

        GETINFO ns/name/<name>

        Looks up given name in list of known OR identities.
        Returns information in Tor's dir-spec format.
        """
        if not self.control:
            return
        try:
            info = self.control.get_info("ns/name/%s" % name)
            print(info)
        except:
            return

    def describe(self, name):
        """Tor node descriptor.

        GETINFO desc/name/<name>

        Retrieves server descriptor for given name.
        """
        if not self.control:
            return
        try:
            info = self.control.get_info("desc/name/%s" % name)
            print(info)
        except:
            return

#######################################################################
## Main
class OniMon:

    def __init__(self, config_file):
        conf.config_dict('onimon', CONFIG_ITEMS, _config_validator)
        self.config = conf.get_config('onimon')
        try:
            self.config.load(config_file)
        except:
            self.config = {}

    def help(self, args):
        """Print program usage information."""

        print("""Usage: %s <command> [options]
    Supported commands:
        config <item>               Value of config item
        version                     Tor version
        process                     Tor process ID
        status                      Node status (1=reachable, 0=unreachable, <0=error)
        fingerprint                 Node fingerprint
        policy [ipv4|ipv6]          Exit policy for given protocol (default: full)
        listeners [or|dir|socks]    Listeners for given connection type (default: any)
        address                     Outbound IP address as used by Tor
        traffic [down|up]           Total traffic for given direction (default: both)
        nodes                       Full list of known OR identities
        lookup <name>               Information on given node
        describe <name>             Server descriptor for given node
    """ % args[0])

    def run(self, args):
        if len(args) < 2:
            self.help(args)
            return

        query = args[1].lower()

        self.controller = OniMonControl(self.config)

        #if query == "test":
        #    cmd = args[2] if len(args) > 2 else "test"
        #    print(self.controller.get_info(cmd))
        #    return

        if query in COMMANDS:
            options, required = COMMANDS[query]
            args = [ a.lower() for a in args[2:] ]

            if len(args) >= required and len(args) <= options:
                func = getattr(self.controller, query)
                func(*args)
                return

        self.help(args)
        return
